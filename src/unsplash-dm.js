import {LitElement, html} from 'lit-element';

export class UnsplashDm extends LitElement {
	static get is() {
		return 'unsplash-dm';
	}

	 // Declare properties
  static get properties() {
    return {
      searchKey: {
        type: String,
        attribute: 'search-key'
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.searchKey = '';
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('dm-component-shared-styles').cssText}
    `;
  }

  // Modifica el método shouldUpdate para permitir que los cambios
  // se den solo si cuando se actualiza cierta propiedad
  shouldUpdate(changedProperties) {
    return changedProperties.has('searchKey');
  }

  // Una vez actualizado el componente, procedemos a verificar los datos actualizados
  updated(changedProperties) {
    this._prepareData();
  }

  // En este método, verificamos la existencia de los datos y que no estén vacios
  _prepareData() {
    const data = this.searchKey;
    if (data && data.length !== 0) {
      this._constructData();
    }
  }

  // Damos la forma a los datos
  async _constructData() {
    const query = this.searchKey.toLowerCase().replace(/ /g, '_');
    const response = await fetch(
          `https://api.unsplash.com/search/photos?page=1&query=${query}?&client_id=dfb3f81ecbddcb11c864491935439d1193e3dcc7f99540b7d6c9ec3915b56fcd`
          );
    const data = await response.json();
    const result = {
      data: await data.results.map(photo => {
        return {
          img_url: photo.urls.small,
          title: photo.description,
          text: photo.alt_description,
          icons : [
            {
              name : 'icons:favorite',
              amount: photo.likes,
              event: 'like-clicked'
            }
          ]
        }
      })
    };
    this._sendData(result);
  }

  // Enviamos los datos de forma nativa
  _sendData(data) {
    this.dispatchEvent(new CustomEvent('unsplash-dm-send-data', {
      bubbles: true,
      composed: true,
      detail: data
    }));
  }
}

customElements.define(UnsplashDm.is, UnsplashDm);